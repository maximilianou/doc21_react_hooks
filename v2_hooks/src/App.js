import React from 'react';
import NameWith from './NameWith';
const App = () => {
  return (
    <div className="App">
      <header className="App-header">
          Learn React
          <NameWith />
      </header>
    </div>
  );
}

export default App;
