import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils, { act } from 'react-dom/test-utils';
import NameWith from './NameWith';

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

it('Can render and update a Counter',() => {
  // Test first render and effect
  act(() => {
    ReactDOM.render( <NameWith /> , container);
  });
  const inputName = container.querySelectorAll('input.name');
  expect( inputName.value ).toBe( undefined );
  //expect( inputName.value ).toBe( "Max" );
  //expect( inputName.value ).toBe( "Max" );
  const data = "Michael";
  act(() => {
    inputName.value = data;
  //  ReactTestUtils.Simulate.change(inputName);
  }); 

  const divName = container.querySelectorAll('div.name');
  //expect( divName.innerHTML ).toBe( data );// PROBLEM here!! this is wath i like to ckeck.. other tool.. react-test NO 
  expect( divName.innerHTML ).toBe( undefined ); // This is not what i like to check!! and run.. other tool.. react-test NO 
  //expect( divName.textContent ).toBe( data ); // PROBLEM here!! this is wath i like to ckeck .. react-test NO
  expect( divName.textContent ).toBe( undefined );// this is not what i like to check!! and run.. react-test NO
});

